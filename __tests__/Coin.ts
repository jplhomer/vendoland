import Coin, { QUARTER } from "../src/Coin";

test("provides dimensions", () => {
  const quarter = new Coin(QUARTER);

  expect(quarter.dimensions).toMatchObject(QUARTER);
});
