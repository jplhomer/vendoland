import Product from "../src/Product";

test("has a name and a price", () => {
  const cola = new Product("Cola", 100);

  expect(cola.name).toBe("Cola");
  expect(cola.price).toBe(100);
});
