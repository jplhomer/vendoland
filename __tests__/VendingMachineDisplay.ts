import VendingMachineDisplay from "../src/VendingMachineDisplay";
import VendingMachine from "../src/VendingMachine";
import Coin, { QUARTER, NICKEL, DIME } from "../src/Coin";

describe("a vending machine display", () => {
  let vm;
  let d;

  beforeEach(() => {
    d = new VendingMachineDisplay();
  });

  test("shows a default message", () => {
    expect(d.message()).toBe("INSERT COIN");
  });

  test("changes from default message to number amount upon insertion", () => {
    expect(d.message()).toBe("INSERT COIN");

    expect(d.message(25)).toBe("$0.25");
  });

  test("changes the number amount displayed after each insertion", () => {
    expect(d.message(5)).toBe("$0.05");

    expect(d.message(15)).toBe("$0.15");
  });

  test("shows trailing zeroes", () => {
    expect(d.message(10)).toBe("$0.10");

    expect(d.message(100)).toBe("$1.00");
  });

  test("shows a thank you message after completed purchase", () => {
    expect(d.message()).toBe("INSERT COIN");

    d.completePurchase();

    expect(d.message()).toBe("THANK YOU");
  });

  test("shows total price reminder", () => {
    d.showPrice(100);

    expect(d.message()).toBe("PRICE $1.00");
    expect(d.message()).toBe("INSERT COIN");

    expect(d.message(25)).toBe("$0.25");

    d.showPrice(100);

    expect(d.message(25)).toBe("PRICE $1.00");
    expect(d.message(25)).toBe("$0.25");
  });
});
