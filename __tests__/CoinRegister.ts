import CoinRegister from "../src/CoinRegister";
import VendingMachine from "../src/VendingMachine";
import Coin, { QUARTER, DIME, NICKEL } from "../src/Coin";
import Product from "../src/Product";

describe("a coin register", () => {
  let register: CoinRegister;
  const penny = new Coin({ size: 1, weight: 1 });

  const quarter = new Coin(QUARTER);
  const dime = new Coin(DIME);
  const nickel = new Coin(NICKEL);

  const apple = new Product("Apple", 25);
  const chips = new Product("Chips", 50);
  const candy = new Product("candy", 65);

  beforeEach(() => {
    register = new CoinRegister();
  });

  test("rejects invalid coins, like pennies", () => {
    expect(register.insert(penny)).toBeFalsy();
  });

  test("updates the total when a coin is inserted", () => {
    register.insert(quarter);
    expect(register.total).toBe(25);

    register.insert(quarter);
    expect(register.total).toBe(50);

    register.insert(nickel);
    expect(register.total).toBe(55);

    register.insert(dime);
    expect(register.total).toBe(65);
  });

  test("places rejected coins in the coin return", () => {
    expect(register.returnSlot).toBeInstanceOf(Array);
    expect(register.returnSlot).toHaveLength(0);

    register.insert(penny);

    expect(register.returnSlot).toHaveLength(1);
    expect(register.returnSlot[0]).toEqual(penny);
  });

  test("does not place valid coins in the coin return", () => {
    expect(register.returnSlot).toHaveLength(0);

    register.insert(quarter);

    expect(register.returnSlot).toHaveLength(0);
  });

  test("tracks coins when inserted", () => {
    register.insert(quarter);

    expect(register.inserted).toHaveLength(1);
    expect(register.inserted[0]).toEqual(quarter);

    register.insert(dime);

    expect(register.inserted).toHaveLength(2);
    expect(register.inserted[1]).toEqual(dime);
  });

  test("increases total when new coins inserted", () => {
    expect(register.total).toBe(0);

    register.insert(quarter);
    expect(register.total).toBe(25);

    register.insert(dime);
    expect(register.total).toBe(35);
  });

  test("removes coins from inserted queue when stored", () => {
    register.insert(quarter);
    expect(register.inserted).toHaveLength(1);

    register.store(apple);

    expect(register.inserted).toHaveLength(0);
  });

  test("resets the total to 0 after store transaction", () => {
    register.insert(quarter);
    expect(register.total).toBe(25);

    register.store(apple);

    expect(register.total).toBe(0);
  });

  describe("returns correct change for a $0.50 product", () => {
    afterEach(() => {
      register.clearReturn();
    });

    test("when given $0.75, a quarter is returned", () => {
      register.insert(quarter);
      register.insert(quarter);
      register.insert(quarter);

      register.store(chips);

      expect(register.returnSlot).toHaveLength(1);
      expect(register.getCoinValue(register.returnSlot[0])).toBe(25);
    });

    test("when given $0.55, a nickel is returned", () => {
      register.insert(quarter);
      register.insert(quarter);
      register.insert(nickel);

      register.store(chips);
      expect(register.returnSlot).toHaveLength(1);
      expect(register.getCoinValue(register.returnSlot[0])).toBe(5);
    });

    test("when given $0.65, a dime and a nickel is returned", () => {
      register.insert(quarter);
      register.insert(quarter);
      register.insert(dime);
      register.insert(nickel);

      register.store(chips);
      expect(register.returnSlot).toHaveLength(2);
      expect(register.getCoinValue(register.returnSlot[0])).toBe(10);
      expect(register.getCoinValue(register.returnSlot[1])).toBe(5);
    });

    test("when given $0.70, two dimes are returned", () => {
      register.insert(quarter);
      register.insert(quarter);
      register.insert(dime);
      register.insert(dime);

      register.store(chips);
      expect(register.returnSlot).toHaveLength(2);
      expect(register.getCoinValue(register.returnSlot[0])).toBe(10);
      expect(register.getCoinValue(register.returnSlot[1])).toBe(10);
    });

    test("when given $0.90, a quarter, a dime and a nickel are returned", () => {
      register.insert(quarter);
      register.insert(quarter);
      register.insert(quarter);
      register.insert(dime);
      register.insert(nickel);

      register.store(chips);
      expect(register.returnSlot).toHaveLength(3);
      expect(register.getCoinValue(register.returnSlot[0])).toBe(25);
      expect(register.getCoinValue(register.returnSlot[1])).toBe(10);
      expect(register.getCoinValue(register.returnSlot[2])).toBe(5);
    });
  });

  test("returns coins when requested", () => {
    register.insert(quarter);

    const coins = register.return();

    expect(coins).toBeInstanceOf(Array);
    expect(register.total).toBe(0);
    expect(coins).toHaveLength(1);
    expect(coins[0]).toEqual(quarter);
  });

  test("knows when can only make exact change", () => {
    register = new CoinRegister(1);

    expect(register.canMakeChange()).toBeFalsy();
  });

  test("change available decreases after change is made", () => {
    register = new CoinRegister(2);

    expect(register.canMakeChange()).toBeTruthy();

    register.insert(quarter);
    register.insert(quarter);
    register.insert(quarter);

    register.store(candy);

    expect(register.canMakeChange()).toBeFalsy();
  });
});
