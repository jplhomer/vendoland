import VendingMachine from "../src/VendingMachine";
import Coin, { QUARTER, DIME, NICKEL } from "../src/Coin";
import VendingMachineDisplay from "../src/VendingMachineDisplay";
import Product from "../src/Product";
import CoinRegister from "../src/CoinRegister";

describe("a vending machine", () => {
  let vm: VendingMachine;
  const quarter = new Coin(QUARTER);
  const dime = new Coin(DIME);
  const nickel = new Coin(NICKEL);
  const penny = new Coin({ size: 1, weight: 1 });

  const chips = new Product("Chips", 50);
  const candy = new Product("Candy", 65);
  const cola = new Product("Cola", 100);

  beforeEach(() => {
    vm = new VendingMachine();
  });

  test("accepts valid coins", () => {
    expect(vm.insert(quarter)).toBeTruthy();

    expect(vm.insert(dime)).toBeTruthy();

    expect(vm.insert(nickel)).toBeTruthy();
  });

  test("rejects invalid coins, like pennies", () => {
    expect(vm.insert(penny)).toBeFalsy();
  });

  test("updates the total when a coin is inserted", () => {
    vm.insert(quarter);
    expect(vm.totalInserted).toBe(25);

    vm.insert(quarter);
    expect(vm.totalInserted).toBe(50);

    vm.insert(nickel);
    expect(vm.totalInserted).toBe(55);

    vm.insert(dime);
    expect(vm.totalInserted).toBe(65);
  });

  test("has a VendingMachineDisplay", () => {
    expect(vm.display).toBeInstanceOf(VendingMachineDisplay);
  });

  test("places rejected coins in the coin return", () => {
    expect(vm.register.returnSlot).toBeInstanceOf(Array);
    expect(vm.register.returnSlot).toHaveLength(0);

    vm.insert(penny);

    expect(vm.register.returnSlot).toHaveLength(1);
    expect(vm.register.returnSlot[0]).toEqual(penny);
  });

  test("does not place valid coins in the coin return", () => {
    expect(vm.register.returnSlot).toHaveLength(0);

    vm.insert(quarter);

    expect(vm.register.returnSlot).toHaveLength(0);
  });

  describe("with a stocked cola", () => {
    beforeEach(() => {
      vm = new VendingMachine([
        {
          product: cola,
          quantity: 1
        }
      ]);
    });

    test("selects and dispenses product when there is enough money", () => {
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);

      expect(vm.message()).toBe("$1.00");
      expect(vm.select(cola)).toBeTruthy();

      expect(vm.message()).toBe("THANK YOU");
      expect(vm.message()).toBe("INSERT COIN");
    });

    test("does not dispense product when there is not enough money", () => {
      expect(vm.message()).toBe("INSERT COIN");

      vm.insert(quarter);

      expect(vm.message()).toBe("$0.25");

      expect(vm.select(cola)).toBeFalsy();

      expect(vm.message()).toBe("PRICE $1.00");
    });
  });

  test("returns correct change after selecting a cheaper product", () => {
    vm = new VendingMachine([
      {
        product: chips,
        quantity: 2
      }
    ]);

    vm.insert(quarter);
    vm.insert(quarter);
    vm.insert(quarter);

    expect(vm.message()).toBe("$0.75");

    vm.select(chips);
    expect(vm.register.returnSlot).toHaveLength(1);
    expect(vm.register.getCoinValue(vm.register.returnSlot[0])).toBe(25);

    vm.clearCoinReturn();

    vm.insert(quarter);
    vm.insert(quarter);
    vm.insert(nickel);

    vm.message(); // Flush the thank you message

    expect(vm.message()).toBe("$0.55");

    vm.select(chips);
    expect(vm.register.returnSlot).toHaveLength(1);
    expect(vm.register.getCoinValue(vm.register.returnSlot[0])).toBe(5);
  });

  test("returns coins when requested", () => {
    vm.insert(quarter);

    const coins = vm.returnCoins();

    expect(coins).toBeInstanceOf(Array);
    expect(vm.totalInserted).toBe(0);
    expect(coins).toHaveLength(1);
    expect(coins[0]).toEqual(quarter);
  });

  describe("with no chips left", () => {
    beforeEach(() => {
      vm = new VendingMachine([
        {
          product: chips,
          quantity: 0
        }
      ]);
    });

    test("shows sold out when customer selects a sold out product", () => {
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);

      expect(vm.select(chips)).toBeFalsy();
      expect(vm.message()).toBe("SOLD OUT");
      expect(vm.message()).toBe("$1.00");

      vm.returnCoins();

      expect(vm.select(chips)).toBeFalsy();
      expect(vm.message()).toBe("SOLD OUT");
      expect(vm.message()).toBe("INSERT COIN");
    });
  });

  describe("with some chips left", () => {
    beforeEach(() => {
      vm = new VendingMachine([
        {
          product: chips,
          quantity: 2
        }
      ]);
    });

    test("inventory is reduced when a product is selected", () => {
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);

      expect(vm.select(chips)).toBeTruthy();

      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);

      expect(vm.select(chips)).toBeTruthy();

      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);
      vm.insert(quarter);

      expect(vm.select(chips)).toBeFalsy();
    });
  });

  test("displays Exact Change Only when register cannot make change", () => {
    vm.register = new CoinRegister(1);

    expect(vm.message()).toBe("EXACT CHANGE ONLY");
  });

  test("change available decreases after change is made", () => {
    vm = new VendingMachine([
      {
        product: candy,
        quantity: 10
      }
    ]);

    vm.register = new CoinRegister(2);

    expect(vm.message()).toBe("INSERT COIN");

    vm.insert(quarter);
    vm.insert(quarter);
    vm.insert(quarter);

    vm.select(candy);

    expect(vm.message()).toBe("THANK YOU");
    expect(vm.message()).toBe("EXACT CHANGE ONLY");
  });
});
