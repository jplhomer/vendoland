# Vendoland

This is a vending machine! It's written in [Typescript](https://www.typescriptlang.org/) and tested with [Jest](https://facebook.github.io/jest/).

## Building

Ensure you have Node >= v8.6.0 and Yarn or NPM installed.

```sh
yarn
```

## Testing

```sh
yarn test

# Generate coverage report:
yarn test --coverage

# Watch for changes:
yarn test --watch
```
