import Coin, { QUARTER, NICKEL, DIME, CoinDimension } from "./Coin";
import VendingMachineDisplay from "./VendingMachineDisplay";
import Product from "./Product";
import CoinRegister from "./CoinRegister";

interface ProductList {
  /**
   * The product added to the vending machine
   */
  product: Product;

  /**
   * How many products are added to the vending machine.
   */
  quantity: number;
}

export default class VendingMachine {
  public display = new VendingMachineDisplay();

  public register = new CoinRegister();

  public inventory: WeakMap<Product, number> = new WeakMap();

  constructor(products: ProductList[] = []) {
    products.forEach(({ product, quantity }) => {
      this.inventory.set(product, quantity);
    });
  }

  /**
   * Insert a coin into the vending machine.
   * @param coin The coin to be inserted
   * @return boolean Whether the coin was accepted.
   */
  insert(coin: Coin): boolean {
    return this.register.insert(coin);
  }

  /**
   * Select a product from the vending machine.
   */
  select(product: Product): boolean {
    if (!this.inventory.get(product)) {
      this.display.soldOut();
      return false;
    }

    if (this.totalInserted >= product.price) {
      this.display.completePurchase();
      this.register.store(product);
    } else {
      this.display.showPrice(product.price);

      return false;
    }

    this.reduceInventory(product);

    return true;
  }

  /**
   * Clear the coin return.
   */
  clearCoinReturn(): void {
    this.register.clearReturn();
  }

  /**
   * Return the coins inserted into the machine.
   */
  returnCoins(): Coin[] {
    return this.register.return();
  }

  /**
   * Get the current display's message.
   */
  message(): string {
    return this.display.message(
      this.totalInserted,
      !this.register.canMakeChange()
    );
  }

  get totalInserted(): number {
    return this.register.total;
  }

  /**
   * Reduce product's inventory
   */
  protected reduceInventory(product: Product): void {
    let count = this.inventory.get(product);
    this.inventory.set(product, Math.max(--count, 0));
  }
}
