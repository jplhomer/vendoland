import VendingMachine from "./VendingMachine";
import Coin, { QUARTER, DIME, NICKEL, CoinDimension } from "./Coin";
import Product from "./Product";

interface CoinDefinition {
  /**
   * The coin's size and weight dimensions.
   */
  dimension: CoinDimension;

  /**
   * The value of that coin, in cents.
   */
  value: number;
}

export default class CoinRegister {
  public COIN_VALUES: CoinDefinition[] = [
    {
      dimension: QUARTER,
      value: 25
    },
    {
      dimension: DIME,
      value: 10
    },
    {
      dimension: NICKEL,
      value: 5
    }
  ];

  /**
   * Total inserted into the register.
   */
  public total: number = 0;

  /**
   * Coins that have been returned.
   */
  public returnSlot: Coin[] = [];

  /**
   * Coins inserted into the register.
   */
  public inserted: Coin[] = [];

  /**
   * Coins available to be used as change.
   */
  protected changeAvailable: Coin[] = [];

  protected MINIMUM_COINS_REQUIRED = 2;

  /**
   * Create a coin register instance.
   * @param change The number of each coin to start out with for change.
   */
  constructor(change: number = 10) {
    this.COIN_VALUES.forEach(({ dimension }) => {
      for (let index = 0; index < change; index++) {
        this.changeAvailable.push(new Coin(dimension));
      }
    });
  }

  /**
   * Insert a coin into the register
   * @param coin Coin
   */
  insert(coin: Coin) {
    if (!this.isValid(coin)) {
      this.returnSlot.push(coin);
      return false;
    }

    this.inserted.push(coin);
    this.total += this.getCoinValue(coin);

    return true;
  }

  /**
   * Store the money entered into the register and make change.
   */
  store(product: Product): void {
    this.makeChange(product);
    this.inserted = [];
    this.total = 0;
  }

  /**
   * Clear the coin return slot.
   */
  clearReturn(): void {
    this.returnSlot = [];
  }

  /**
   * Return coins inserted.
   */
  return(): Coin[] {
    const coins = this.inserted;

    this.inserted = [];
    this.total = 0;

    return coins;
  }

  /**
   * Whether the machine can make change.
   * NOTE: This arbitrarily checks for at least 2 coins of each type
   * in order to pass. This probably isn't right, but it's a start.
   */
  canMakeChange(): boolean {
    return this.COIN_VALUES.every(({ dimension }) => {
      return (
        this.changeAvailable.filter(
          coin => JSON.stringify(coin) === JSON.stringify(dimension)
        ).length >= this.MINIMUM_COINS_REQUIRED
      );
    });
  }

  /**
   * Determine whether coin is valid.
   * @param coin Coin
   */
  protected isValid(coin: Coin): boolean {
    return !!this.getCoinDefinition(coin);
  }

  /**
   * Get this coin's value, in cents.
   * @param coin Coin
   */
  public getCoinValue(coin: Coin) {
    return this.getCoinDefinition(coin).value;
  }

  /**
   * Get the definition of the given coin, defined on the class.
   * @param coin | boolean
   */
  protected getCoinDefinition(coin) {
    return this.COIN_VALUES.find(v => {
      return JSON.stringify(v.dimension) === JSON.stringify(coin);
    });
  }

  /**
   * Make change for a product. Places coins in the return slot.
   * @param product Product to make change for.
   */
  protected makeChange(product: Product): void {
    if (this.total <= product.price) {
      return;
    }

    let remainder = this.total - product.price;
    let i = 0;

    while (remainder >= this.getCoinValue(new Coin(NICKEL))) {
      let coin = this.COIN_VALUES[i];
      let count = Math.min(
        Math.floor(remainder / coin.value),
        this.coinsOfTypeAvailableInChange(coin.dimension)
      );
      remainder -= count * coin.value;

      Array(count)
        .fill("")
        .forEach(() => {
          let changeCoin = this.getCoinOfTypeFromChangeAvailable(
            coin.dimension
          );
          this.returnSlot.push(changeCoin);
        });
      i++;
    }
  }

  /**
   * Get the number of coins available in change for a given dimension.
   * @param dimension The dimension describing the coin.
   */
  protected coinsOfTypeAvailableInChange(dimension: CoinDimension): number {
    return this.changeAvailable.filter(
      c => JSON.stringify(c) === JSON.stringify(dimension)
    ).length;
  }

  /**
   * Get a coin (and remove it) from the change available.
   * @param dimension The dimension describing the desired coin.
   */
  protected getCoinOfTypeFromChangeAvailable(dimension: CoinDimension): Coin {
    const index = this.changeAvailable.findIndex(
      c => JSON.stringify(c) === JSON.stringify(dimension)
    );

    return this.changeAvailable.splice(index, 1)[0];
  }
}
