export interface CoinDimension {
  /**
   * The diameter of the coin, in millimeters.
   */
  size: number;

  /**
   * The weight of the coin, in grams.
   */
  weight: number;
}

export default class Coin {
  public size: number;
  public weight: number;

  /**
   * Create a new coin instance.
   *
   * @param size The size of the coin (in millimeters).
   * @param weight The weight of the coin (in grams).
   */
  constructor({ size, weight }: CoinDimension) {
    this.size = size;
    this.weight = weight;
  }

  get dimensions(): CoinDimension {
    return {
      size: this.size,
      weight: this.weight
    };
  }
}

export const QUARTER: CoinDimension = {
  size: 24,
  weight: 6
};

export const DIME: CoinDimension = {
  size: 18,
  weight: 2
};

export const NICKEL: CoinDimension = {
  size: 21,
  weight: 5
};
