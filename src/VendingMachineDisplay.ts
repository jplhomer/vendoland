import VendingMachine from "./VendingMachine";

export default class VendingMachineDisplay {
  /**
   * Controls the state of the display thank you message.
   */
  private hasCompletedPurchase: boolean = false;

  /**
   * The price that needs to be displayed on the screen.
   */
  private priceDisplayed: number = 0;

  /**
   * Whether the current product has sold out.
   */
  private isSoldOut: boolean = false;

  /**
   * Get the message on the display.
   */
  public message(total: number = 0, exactChangeOnly: boolean = false): string {
    if (this.isSoldOut) {
      this.isSoldOut = false;
      return "SOLD OUT";
    }

    if (this.hasCompletedPurchase) {
      this.hasCompletedPurchase = false;
      return "THANK YOU";
    }

    if (this.priceDisplayed) {
      const message = `PRICE ${this.formatMoney(this.priceDisplayed)}`;
      this.priceDisplayed = 0;
      return message;
    }

    if (!total) {
      if (exactChangeOnly) {
        return "EXACT CHANGE ONLY";
      }

      return "INSERT COIN";
    }

    return this.formatMoney(total);
  }

  /**
   * Mark the display as having completed a purchase.
   */
  public completePurchase(): void {
    this.hasCompletedPurchase = true;
  }

  /**
   * Show price on the display.
   */
  public showPrice(price: number) {
    this.priceDisplayed = price;
  }

  /**
   * Show Sold Out message on display.
   */
  public soldOut(): void {
    this.isSoldOut = true;
  }

  /**
   * Format money in cents for the display in dollars.
   * @param cents Cents
   */
  private formatMoney(cents): string {
    let dollars = cents / 100;
    return `$${dollars.toFixed(2)}`;
  }
}
