interface ProductOptions {}

export default class Product {
  /**
   * The name of the product.
   */
  public name: string;

  /**
   * The price of the product, in cents.
   */
  public price: number;

  constructor(name: string, price: number) {
    this.name = name;
    this.price = price;
  }
}
